package com.dtuPay.usermanager.rest.dto;

/**
 * Class representation of a request
 * for various user management actions 
 * independent of customer/merchant status (server side).
 * 
 * @author Hubert Baumeister
 *
 */
public class UserRepresentation {
	
    /**
     * Identifier of the requested user.
     */
	private String cpr;
	
	/**
	 * First name of the requested user.
	 */
	private String firstName;
	
	/**
	 * Surname of the requested user.
	 */
	private String lastName;
	
	/**
	 * Bank account ID of the requested user.
	 */
	private String account;

	public String getCpr() {
		return cpr;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public String getAccount() {
	    return account;
	}

	public void setCpr(String cpr) {
		this.cpr = cpr;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setAccount(String account) {
	    this.account = account;
	}

}