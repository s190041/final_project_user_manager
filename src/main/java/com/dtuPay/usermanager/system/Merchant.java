package com.dtuPay.usermanager.system;

/**
 * Class representation of a DTUPay merchant.
 * 
 * @author Hubert Baumeister
 *
 */
public class Merchant extends DtuPayUser {
    
    public Merchant(String cpr, String firstName, String lastName, String account) {
        super(cpr, firstName, lastName, account);
    }

}
