package com.dtuPay.usermanager.system;

/**
 * Class representation of a DTUPay customer.
 * 
 * @author Hubert Baumeister
 *
 */
public class Customer extends DtuPayUser {

    public Customer(String cpr, String firstName, String lastName, String account) {
        super(cpr, firstName, lastName, account);
    }

}
