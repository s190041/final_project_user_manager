package behaviourtests;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class UserService {

    WebTarget baseUrl;
    private String tokenServicePath = "/";
    
    public UserService() {
        Client client = ClientBuilder.newClient();
        this.baseUrl = client.target("http://localhost:8080/");
    }
}
