package dtupay.rest.dto;

public class Token {
	
	private String value;
	
	public Token() {}
	
	public Token(String c) {
		value = c;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Token)) {
			return false;
		}
		Token otherToken = (Token) other;
		return value.equals(otherToken.value);
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}
	
	@Override
	public String toString() {
		return value;
	}

}
