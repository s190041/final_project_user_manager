package dtupay.rest.dto;


import java.util.List;

import javax.ws.rs.Path;

import dtu.ws.fastmoney.BankServiceService;
import dtupay.steps.DtuPay;


@Path("/somepath")
public class SomepathResource {
	
	private static DtuPay dtuPay = new DtuPay(new BankServiceService().getBankServicePort());
	
	@Path("more1")
	public String registerCustomer(DtuPayUserRepresentation c) {
		dtuPay.registerCustomer(c.getCpr()
				, c.getFirstName()
				, c.getLastName()
				, c.getAccount());
		return c.getCpr();
	}
	
	@Path("more2")
	public String registerMerchant(DtuPayUserRepresentation m) {
		dtuPay.registerMerchant(m.getCpr()
				, m.getFirstName()
				, m.getLastName()
				, m.getAccount());
		return m.getCpr();
	}
	
	@Path("more3")
	public List<Token> requestTokens(TokenRequest r) {
		return dtuPay.requestTokens(r.getCpr(), r.getNumber());
	}
	
	@Path("more4")
	public boolean pay(PaymentRequest p) {
		return dtuPay.payWithToken(p.getAmount()
				, p.getMerchantCpr()
				, p.getToken()
				, p.getDescription());
	}
}
