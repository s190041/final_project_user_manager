package dtupay.rest.dto;

public class TokenRequest {
	
	private String cpr;
	private int number;
	
	public String getCpr() {
		return cpr;
	}
	public void setCpr(String cpr) {
		this.cpr = cpr;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	} 

}
