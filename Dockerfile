FROM adoptopenjdk:8-jre-hotspot
COPY target/user_manager-thorntail.jar /usr/src/
WORKDIR /usr/src/
EXPOSE 8080:8080
CMD java -Djava.net.preferIPv4Stack=true\
         -Djava.net.preferIPv4Addresses=true\
         -Xmx64m -jar user_manager-thorntail.jar
