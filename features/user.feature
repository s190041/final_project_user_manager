Feature: User feature

  Scenario: Customer Register
    Given a customer with a bank account
    When the customer register in dtuPay
    Then the customer is registered successfully
    
  Scenario: Merchant Register
    Given a merchant with a bank account
    When the merchant register in dtuPay
    Then the merchant is registered successfully    